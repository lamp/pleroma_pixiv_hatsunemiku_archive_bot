import {readFileSync, writeFileSync} from "fs";
import {fetch} from "./common.js";

export default class Bot {
	constructor(opts) {
		for (var key in opts) this[key] = opts[key];
	}

	async login() {
		if (this.loginData) return;
		try {
			this.loginData = JSON.parse(readFileSync(`logindata/${this.username}.json`, "utf8"));
			return;
		} catch (error) {}
		console.log(this.username, "logging in");
		var form = new FormData();
		form.append("client_id", this.app.client_id);
		form.append("client_secret", this.app.client_secret);
		form.append("grant_type", "password");
		form.append("username", this.username);
		form.append("password", this.password);
		var res = await fetch(this.url + "/oauth/token", {
			method: "POST",
			body: form
		});
		this.loginData = await res.json();
		writeFileSync(`logindata/${this.username}.json`, JSON.stringify(this.loginData));
	}

	async post({status, visibility = "unlisted", content_type = "text/plain", media_ids = [], sensitive, files, edit}) {
		if (files) {
			media_ids = (await Promise.all(files.map(file => this.uploadFile(file)))).map(d => d.id);
		}
		var form = new FormData();
		form.append("status", status);
		form.append("visibility", visibility);
		form.append("source", "bot");
		form.append("content_type", content_type);
		for (let media_id of media_ids) {
			form.append("media_ids[]", media_id);
		}
		if (sensitive) form.append("sensitive", "true");
		var url = this.url + "/api/v1/statuses";
		if (edit) url += "/" + edit;
		var res = await fetch(url, {
			method: edit ? "PUT" : "POST",
			body: form,
			headers: {
				"Authorization": `${this.loginData.token_type} ${this.loginData.access_token}`
			}
		});
		var json = await res.json();
		console.log(edit ? "edited" : "posted", res.status, json.uri || json);
		return json;
	}

	async uploadFile({data, name}) {
		var form = new FormData();
		form.append("file", data, name);
		var res = await fetch(this.url + "/api/v1/media", {
			method: "POST",
			body: form,
			headers: {
				"Authorization": `${this.loginData.token_type} ${this.loginData.access_token}`
			}
		});
		var json = await res.json();
		console.log("uploaded file", res.status, json.url);
		return json;
	}
}