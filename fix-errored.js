import {readFileSync} from "fs";
import { mikubot, mikubot_nsfw } from "./bots.js";
import { pixivToPleroma } from "./common.js";

// psql -d pleroma39 -t -c "select data from objects where data->>'type' = 'Note' and data->>'actor' like '%pixiv_hatsunemiku%'" -o dump.txt

var dump = readFileSync("dump.txt", "utf8");
dump = dump.trim().split("\n");
dump = dump.filter(line => line.includes("#error"));
dump = dump.map(line => JSON.parse(line.trim()));
dump = dump.filter(obj => obj.content.includes("#error") && !obj.content.includes("HTTP 404"));

console.log(`${dump.length} errored posts`);

for (let {id, content, actor} of dump) {
	let ap_id = (await fetch(id, {redirect: "manual"})).headers.get("Location").split('/').at(-1); // todo uuid to base62 but this works already
	console.log(`${id} => ${ap_id}`);

	let pixiv_id = content.match(/https:\/\/www.pixiv.net\/en\/artworks\/(\d+)/)[1];
	let bot = actor.includes("hentai") ? mikubot_nsfw : mikubot;
	
	await pixivToPleroma(bot, pixiv_id, ap_id);
}