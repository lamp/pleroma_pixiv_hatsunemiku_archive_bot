import {unescape} from 'html-escaper';
import { getPostDataById } from "./pixiv.js";


export async function fetch(url, options, nothrow) {
	for (var a = 0, ma = 10, error; a < ma; a++) {
		console.log(options.method || "GET", url);
		try {
			var res = await global.fetch(url, options);
			error = null;
			break;
		} catch (e) {
			console.error("fetch error", e);
			error = e;
		}
	}
	if (error) throw error;
	if (!res.ok && !nothrow) throw new Error(`HTTP ${res.status} ${await res.text()}`);
	return res;
}

export async function sleep(seconds) {
	await new Promise(resolve => setTimeout(resolve, seconds * 1000));
}

export async function pixivToPleroma(bot, pixiv_id, edit) {
	let url = `https://www.pixiv.net/en/artworks/${pixiv_id}`;
	try {
		let {images, illust} = await getPostDataById(pixiv_id);
		let date = new Date(illust.createDate);
		let dateString = date.toLocaleDateString("en-US", {timeZone: "JST", month: "long", day: "numeric", year: "numeric"});
		let timeString = date.toLocaleTimeString("en-US", {timeZone: "JST", hour12: true, hour: "numeric", "minute": "numeric"});
		let tags = illust.tags.tags.map(tag => `#${tag.tag}`).join(" ");
		if (illust.aiType == 2) tags = `#AIgenerated ${tags}`;
		let status = `<b>${illust.title}</b> / ${illust.userName} / ${dateString} ${timeString}<br>${unescape(illust.description)}<br>${tags}<br>${url}<br>https://www.pixiv.net/en/users/${illust.userId}`;
		if (images.length > 4) {
			status += `<br>⚠ There are ${images.length} images.`;
		}
		if (illust.illustType == 2) {
			status += `<br>⚠ This is ugoria, you have to view on Pixiv.`;
		}
		await bot.post({
			status,
			content_type: "text/html",
			files: images,
			sensitive: Boolean(illust.xRestrict),
			visibility: "public",
			edit
		});
		return true;
	} catch(error) {
		console.error(error.stack);
		await bot.post({
			status: `${url}\n#error\n${error.stack}`,
			visibility: "public",
			edit
		});
	}
}