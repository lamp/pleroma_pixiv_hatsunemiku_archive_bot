import credentials from "./credentials.json" assert { type: 'json' };
import Bot from "./Bot.js";

export var mikubot = new Bot({
	url: "https://mikuobsession.net",
	app: credentials.app,
	username: credentials.accounts[0].username,
	password: credentials.accounts[0].password
});

export var mikubot_nsfw = new Bot({
	url: "https://mikuobsession.net",
	app: credentials.app,
	username: credentials.accounts[1].username,
	password: credentials.accounts[1].password
});


await mikubot.login();
await mikubot_nsfw.login();
