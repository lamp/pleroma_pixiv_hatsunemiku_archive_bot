console.log("start", Date());

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
import { appendFileSync } from "fs";
import { getNewPixivPosts } from "./pixiv.js";
import { pixivToPleroma } from './common.js';
import { mikubot, mikubot_nsfw } from "./bots.js";

var newPosts = await getNewPixivPosts("初音ミク");

for (let post of newPosts) {
	let bot = post.xRestrict ? mikubot_nsfw : mikubot;
	if (await pixivToPleroma(bot, post.id)) {
		appendFileSync(`known_ids/初音ミク.txt`, "\n" + post.id);
	}
}

console.log("end", Date());
